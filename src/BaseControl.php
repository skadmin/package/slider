<?php

declare(strict_types=1);

namespace Skadmin\Slider;

use App\Model\System\ABaseControl;
use Nette\Utils\ArrayHash;
use Nette\Utils\Html;

class BaseControl extends ABaseControl
{
    public const RESOURCE  = 'slider';
    public const DIR_IMAGE = 'slider';

    public function getMenu(): ArrayHash
    {
        return ArrayHash::from([
            'control' => $this,
            'icon'    => Html::el('i', ['class' => 'far fa-fw fa-images']),
            'items'   => ['overview'],
        ]);
    }
}
