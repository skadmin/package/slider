<?php

declare(strict_types=1);

namespace Skadmin\Slider\Components\Admin;

use App\Model\Grid\Traits\IsActive;
use App\Model\System\APackageControl;
use App\Model\System\Flash;
use Nette\ComponentModel\IContainer;
use Nette\Security\User;
use Nette\Utils\Html;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\Slider\BaseControl;
use Skadmin\Slider\Doctrine\Slider\Slider;
use Skadmin\Slider\Doctrine\Slider\SliderFacade;
use Skadmin\Slider\Doctrine\SliderItem\SliderItem;
use Skadmin\Slider\Doctrine\SliderItem\SliderItemFacade;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\GridControls\UI\GridControl;
use SkadminUtils\GridControls\UI\GridDoctrine;
use SkadminUtils\ImageStorage\ImageStorage;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

use function sprintf;

class OverviewItems extends GridControl
{
    use APackageControl;
    use IsActive;

    private SliderItemFacade $facade;
    private Slider           $slider;
    private LoaderFactory    $webLoader;
    private ImageStorage     $imageStorage;

    public function __construct(int $id, SliderFacade $facadeSlider, SliderItemFacade $facade, Translator $translator, ImageStorage $imageStorage, User $user, LoaderFactory $webLoader)
    {
        parent::__construct($translator, $user);

        $this->facade       = $facade;
        $this->imageStorage = $imageStorage;
        $this->webLoader    = $webLoader;

        $this->slider = $facadeSlider->get($id);
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::READ)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/overviewItems.latte');
        $template->render();
    }

    public function getTitle(): SimpleTranslation
    {
        return new SimpleTranslation('slider.overview-items.title - %s', $this->slider->getName());
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [$this->webLoader->createJavaScriptLoader('jQueryUi')];
    }

    protected function createComponentGrid(string $name): GridDoctrine
    {
        $grid = new GridDoctrine($this->getPresenter());

        // DEFAULT
        $grid->setPrimaryKey('id');
        $grid->setDataSource($this->facade->getModelForSlider($this->slider));

        // COLUMNS
        $grid->addColumnText('imagePreview', '')
            ->setRenderer(function (SliderItem $sliderItem): ?Html {
                if ($sliderItem->getImagePreview() !== null) {
                    $imageSrc = $this->imageStorage->fromIdentifier([$sliderItem->getImagePreview(), '120x60', 'exact']);

                    return Html::el('img', [
                        'src'   => sprintf('/%s', $imageSrc->createLink()),
                        'style' => 'max-width: none;',
                    ]);
                }

                return null;
            })->setAlign('center');
        $grid->addColumnText('name', 'grid.slider.overview-items.name')
            ->setRenderer(function (SliderItem $sliderItem): Html {
                if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
                    $link = $this->getPresenter()->link('Component:default', [
                        'package' => new BaseControl(),
                        'render'  => 'edit-item',
                        'id'      => $sliderItem->getId(),
                    ]);

                    $name = Html::el('a', [
                        'href'  => $link,
                        'class' => 'font-weight-bold',
                    ]);
                } else {
                    $name = new Html();
                }

                $name->setText($sliderItem->getName());

                return $name;
            });
        $this->addColumnIsActive($grid, 'slider.overview-items');

        // STYLE
        $grid->getColumn('imagePreview')
            ->getElementPrototype('th')
            ->setAttribute('style', 'width: 1px');

        // FILTER
        $grid->addFilterText('name', 'grid.slider.overview-items.name');
        $this->addFilterIsActive($grid, 'slider.overview-items');

        // ACTION
        if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
            $grid->addAction('edit', 'grid.slider.overview-items.action.edit', 'Component:default', ['id' => 'id'])->addParameters([
                'package' => new BaseControl(),
                'render'  => 'edit-item',
            ])->setIcon('pencil-alt')
                ->setClass('btn btn-xs btn-default btn-primary');
        }

        // TOOLBAR
        if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
            $grid->addToolbarButton('Component:default', 'grid.slider.overview-items.action.new', [
                'package'  => new BaseControl(),
                'render'   => 'edit-item',
                'sliderId' => $this->slider->getId(),
            ])->setIcon('plus')
                ->setClass('btn btn-xs btn-default btn-primary');
        }

        // SORTING
        $grid->setSortable();
        $grid->setSortableHandler($this->link('sort!'));

        return $grid;
    }

    public function handleSort(?string $itemId, ?string $prevId, ?string $nextId): void
    {
        $this->facade->sort($itemId, $prevId, $nextId);

        $presenter = $this->getPresenterIfExists();
        if ($presenter !== null) {
            $presenter->flashMessage('grid.slider.overview-items.action.flash.sort.success', Flash::SUCCESS);
        }

        $this['grid']->reload();
    }
}
