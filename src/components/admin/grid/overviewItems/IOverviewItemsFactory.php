<?php

declare(strict_types=1);

namespace Skadmin\Slider\Components\Admin;

interface IOverviewItemsFactory
{
    public function create(int $id): OverviewItems;
}
