<?php

declare(strict_types=1);

namespace Skadmin\Slider\Components\Admin;

use App\Model\Grid\Traits\IsActive;
use App\Model\System\APackageControl;
use App\Model\System\Constant;
use App\Model\System\Flash;
use Nette\ComponentModel\IContainer;
use Nette\Forms\Container;
use Nette\Security\User;
use Nette\Utils\ArrayHash;
use Nette\Utils\Html;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\Slider\BaseControl;
use Skadmin\Slider\Doctrine\Slider\Slider;
use Skadmin\Slider\Doctrine\Slider\SliderFacade;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\GridControls\UI\GridControl;
use SkadminUtils\GridControls\UI\GridDoctrine;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

use function boolval;
use function intval;

class Overview extends GridControl
{
    use APackageControl;
    use IsActive;

    private SliderFacade  $facade;
    private LoaderFactory $webLoader;

    public function __construct(SliderFacade $facade, Translator $translator, User $user, LoaderFactory $webLoader)
    {
        parent::__construct($translator, $user);

        $this->facade    = $facade;
        $this->webLoader = $webLoader;
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::READ)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/overview.latte');
        $template->render();
    }

    public function getTitle(): string
    {
        return 'slider.overview.title';
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [$this->webLoader->createJavaScriptLoader('jQueryUi')];
    }

    protected function createComponentGrid(string $name): GridDoctrine
    {
        $grid = new GridDoctrine($this->getPresenter());

        // DEFAULT
        $grid->setPrimaryKey('id');
        $grid->setDataSource($this->facade->getModel());

        // COLUMNS
        $grid->addColumnText('name', 'grid.slider.overview.name')
            ->setRenderer(function (Slider $slider): Html {
                if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
                    $link = $this->getPresenter()->link('Component:default', [
                        'package' => new BaseControl(),
                        'render'  => 'overview-items',
                        'id'      => $slider->getId(),
                    ]);

                    $name = Html::el('a', [
                        'href'  => $link,
                        'class' => 'font-weight-bold',
                    ]);
                } else {
                    $name = new Html();
                }

                $name->setText($slider->getName());

                return $name;
            });
        $this->addColumnIsActive($grid, 'slider.overview');
        $grid->addColumnText('items-count', 'grid.slider.overview.items-count')
            ->setRenderer(static function (Slider $slider): int {
                return $slider->getItems()->count();
            })->setAlign('center');

        // FILTER
        $grid->addFilterText('name', 'grid.slider.overview.name');
        $this->addFilterIsActive($grid, 'slider.overview');

        // ACTION
        $grid->addAction('overviewPhotos', 'grid.slider.overview.action.overview-items', 'Component:default', ['id' => 'id'])
            ->addParameters([
                'package' => new BaseControl(),
                'render'  => 'overview-items',
            ])->setIcon('images')
            ->setClass('btn btn-xs btn-outline-primary');

        if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
            // INLINE ADD
            $grid->addInlineAdd()
                ->setPositionTop()
                ->setText($this->translator->translate('grid.slider.overview.action.inline-add'))
                ->onControlAdd[] = [$this, 'onInlineAdd'];
            $grid->getInlineAddPure()
                ->onSubmit[]     = [$this, 'onInlineAddSubmit'];

            // INLINE EDIT
            $grid->addInlineEdit()
                ->onControlAdd[]                        = [$this, 'onInlineEdit'];
            $grid->getInlineEditPure()->onSetDefaults[] = [$this, 'onInlineEditDefaults'];
            $grid->getInlineEditPure()->onSubmit[]      = [$this, 'onInlineEditSubmit'];
        }

        return $grid;
    }

    public function onInlineAdd(Container $container): void
    {
        $container->addText('name', 'grid.slider.overview.name')
            ->setRequired('grid.slider.overview.name.req');
        $container->addSelect('isActive', 'grid.slider.overview.is-active', Constant::DIAL_YES_NO)
            ->setDefaultValue(Constant::YES);
    }

    /**
     * @param ArrayHash<mixed> $values
     */
    public function onInlineAddSubmit(ArrayHash $values): void
    {
        $slider = $this->facade->create($values->name, boolval($values->isActive));

        $message = new SimpleTranslation('grid.slider.overview.action.flash.inline-add.success "%s"', [$slider->getName()]);
        $this->onFlashmessage($message, Flash::SUCCESS);
    }

    public function onInlineEdit(Container $container): void
    {
        $container->addText('name', 'grid.slider.overview.name')
            ->setRequired('grid.slider.overview.name.req');
        $container->addSelect('isActive', 'grid.slider.overview.is-active', Constant::DIAL_YES_NO);
    }

    public function onInlineEditDefaults(Container $container, Slider $slider): void
    {
        $container->setDefaults([
            'name'     => $slider->getName(),
            'isActive' => intval($slider->isActive()),
        ]);
    }

    /**
     * @param ArrayHash<mixed> $values
     */
    public function onInlineEditSubmit(int|string $id, ArrayHash $values): void
    {
        $slider = $this->facade->update(intval($id), $values->name, boolval($values->isActive));

        $message = new SimpleTranslation('grid.slider.overview.action.flash.inline-edit.success "%s"', [$slider->getName()]);
        $this->onFlashmessage($message, Flash::SUCCESS);
    }
}
