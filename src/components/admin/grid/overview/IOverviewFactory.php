<?php

declare(strict_types=1);

namespace Skadmin\Slider\Components\Admin;

interface IOverviewFactory
{
    public function create(): Overview;
}
