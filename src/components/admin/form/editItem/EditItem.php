<?php

declare(strict_types=1);

namespace Skadmin\Slider\Components\Admin;

use App\Model\System\APackageControl;
use App\Model\System\Flash;
use Exception;
use Nette\ComponentModel\IContainer;
use Nette\Security\User as LoggedUser;
use Nette\Utils\ArrayHash;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\Slider\BaseControl;
use Skadmin\Slider\Doctrine\Slider\Slider;
use Skadmin\Slider\Doctrine\Slider\SliderFacade;
use Skadmin\Slider\Doctrine\SliderItem\SliderItem;
use Skadmin\Slider\Doctrine\SliderItem\SliderItemFacade;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;
use SkadminUtils\FormControls\UI\FormWithUserControl;
use SkadminUtils\FormControls\Utils\UtilsFormControl;
use SkadminUtils\ImageStorage\ImageStorage;
use WebLoader\Nette\CssLoader;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

use function intval;

class EditItem extends FormWithUserControl
{
    use APackageControl;

    private LoaderFactory    $webLoader;
    private SliderItemFacade $facade;
    private SliderItem       $sliderItem;
    private SliderFacade     $facadeSlider;
    private ?Slider          $slider = null;
    private ImageStorage     $imageStorage;

    public function __construct(?int $id, SliderItemFacade $facade, SliderFacade $facadeSlider, Translator $translator, LoaderFactory $webLoader, LoggedUser $user, ImageStorage $imageStorage)
    {
        parent::__construct($translator, $user);
        $this->facade       = $facade;
        $this->facadeSlider = $facadeSlider;

        $this->webLoader    = $webLoader;
        $this->imageStorage = $imageStorage;

        $this->sliderItem = $this->facade->get($id);

        if ($this->sliderItem->isLoaded()) {
            $this->slider = $this->sliderItem->getSlider();
        } elseif (isset($_GET['sliderId'])) {
            $this->slider = $this->facadeSlider->get(intval($_GET['sliderId']));
        }
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function getTitle(): SimpleTranslation|string
    {
        if ($this->sliderItem->isLoaded()) {
            return new SimpleTranslation('slider.edit-item.title - %s', $this->sliderItem->getName());
        }

        return 'slider.edit-item.title';
    }

    /**
     * @return CssLoader[]
     */
    public function getCss(): array
    {
        return [
            $this->webLoader->createCssLoader('customFileInput'),
            $this->webLoader->createCssLoader('fancyBox'), // responsive file manager
        ];
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [
            $this->webLoader->createJavaScriptLoader('adminTinyMce'),
            $this->webLoader->createJavaScriptLoader('customFileInput'),
            $this->webLoader->createJavaScriptLoader('skadminForms'),
            $this->webLoader->createJavaScriptLoader('fancyBox'), // responsive file manager
        ];
    }

    public function processOnSuccess(Form $form, ArrayHash $values): void
    {
        // IDENTIFIER
        $identifier = UtilsFormControl::getImagePreview($values->imagePreview, BaseControl::DIR_IMAGE);

        if (isset($values->sliderId)) {
            $this->slider = $this->facadeSlider->get(intval($values->sliderId));
        }

        if ($this->slider === null) {
            throw new Exception('Slider result not found!');
        }

        if ($this->sliderItem->isLoaded()) {
            if ($identifier !== null && $this->sliderItem->getImagePreview() !== null) {
                $this->imageStorage->delete($this->sliderItem->getImagePreview());
            }

            $slider = $this->facade->update(
                $this->sliderItem->getId(),
                $this->slider,
                $values->name,
                $values->content,
                $values->isActive,
                $values->url1,
                $values->url2,
                $values->urlText1,
                $values->urlText2,
                $values->additionalSettings,
                $identifier
            );
            $this->onFlashmessage('form.slider.edit-item.flash.success.update', Flash::SUCCESS);
        } else {
            $slider = $this->facade->create(
                $this->slider,
                $values->name,
                $values->content,
                $values->isActive,
                $values->url1,
                $values->url2,
                $values->urlText1,
                $values->urlText2,
                $values->additionalSettings,
                $identifier
            );
            $this->onFlashmessage('form.slider.edit-item.flash.success.create', Flash::SUCCESS);
        }

        if ($form->isSubmitted()->name === 'sendBack') {
            $this->processOnBack();
        }

        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'edit-item',
            'id'      => $slider->getId(),
        ]);
    }

    public function processOnBack(): void
    {
        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'overview-items',
            'id'      => $_POST['sliderId'] ?? $this->sliderItem->getSlider()->getId(),
        ]);
    }

    public function render(): void
    {
        $template               = $this->getComponentTemplate();
        $template->imageStorage = $this->imageStorage;
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/editItem.latte');

        $template->sliderItem = $this->sliderItem;
        $template->render();
    }

    protected function createComponentForm(): Form
    {
        $form = new Form();
        $form->setTranslator($this->translator);

        if (! $this->sliderItem->isLoaded()) {
            $form->addHidden('sliderId', $this->slider !== null ? (string) $this->slider->getId() : '');
        }

        // INPUT
        $form->addText('name', 'form.slider.edit-item.name')
            ->setRequired('form.slider.edit-item.name.req');
        $form->addCheckbox('isActive', 'form.slider.edit-item.is-active')
            ->setDefaultValue(true);
        $form->addImageWithRFM('imagePreview', 'form.slider.edit-item.image-preview');

        // URL
        $form->addText('url1', 'form.slider.edit-item.url-1');
        $form->addText('url2', 'form.slider.edit-item.url-2');
        $form->addText('urlText1', 'form.slider.edit-item.url-text-1')
            ->setHtmlAttribute('placeholder', 'form.slider.edit-item.url-text-1.placeholder');
        $form->addText('urlText2', 'form.slider.edit-item.url-text-2')
            ->setHtmlAttribute('placeholder', 'form.slider.edit-item.url-text-2.placeholder');

        $form->addInlineArray('additionalSettings', 'form.slider.edit-item.additional-settings');

        // TEXT
        $form->addTextArea('content', 'form.slider.edit-item.content');

        // BUTTON
        $form->addSubmit('send', 'form.slider.edit-item.send');
        $form->addSubmit('sendBack', 'form.slider.edit-item.send-back');
        $form->addSubmit('back', 'form.slider.edit-item.back')
            ->setValidationScope([])
            ->onClick[] = [$this, 'processOnBack'];

        // DEFAULT
        $form->setDefaults($this->getDefaults());

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }

    /**
     * @return mixed[]
     */
    private function getDefaults(): array
    {
        if (! $this->sliderItem->isLoaded()) {
            return [];
        }

        return [
            'name'               => $this->sliderItem->getName(),
            'content'            => $this->sliderItem->getContent(),
            'url1'               => $this->sliderItem->getUrl1(),
            'url2'               => $this->sliderItem->getUrl2(),
            'urlText1'           => $this->sliderItem->getUrlText1(),
            'urlText2'           => $this->sliderItem->getUrlText2(),
            'isActive'           => $this->sliderItem->isActive(),
            'additionalSettings' => $this->sliderItem->getAdditionalSettings(),
        ];
    }
}
