<?php

declare(strict_types=1);

namespace Skadmin\Slider\Components\Admin;

interface IEditItemFactory
{
    public function create(?int $id = null): EditItem;
}
