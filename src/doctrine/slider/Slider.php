<?php

declare(strict_types=1);

namespace Skadmin\Slider\Doctrine\Slider;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Skadmin\Slider\Doctrine\SliderItem\SliderItem;
use SkadminUtils\DoctrineTraits\Entity;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class Slider
{
    use Entity\Id;
    use Entity\Name;
    use Entity\IsActive;

    /** @var ArrayCollection|Collection|SliderItem[] */
    #[ORM\OneToMany(targetEntity: SliderItem::class, mappedBy: 'slider')]
    #[ORM\OrderBy(['sequence' => 'ASC'])]
    private ArrayCollection|Collection|array $items;

    public function __construct()
    {
        $this->items = new ArrayCollection();
    }

    public function update(string $name, bool $isActive): void
    {
        $this->name     = $name;
        $this->isActive = $isActive;
    }

    /**
     * @return ArrayCollection|Collection|SliderItem[]
     */
    public function getItems(bool $onlyActive = false, ?int $limit = null): ArrayCollection|Collection|array
    {
        $criteria = Criteria::create();

        if ($onlyActive) {
            $criteria->where(Criteria::expr()->eq('isActive', true));
        }

        if ($limit !== null) {
            $criteria->setMaxResults($limit);
        }

        return $this->items->matching($criteria);
    }
}
