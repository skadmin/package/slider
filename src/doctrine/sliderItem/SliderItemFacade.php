<?php

declare(strict_types=1);

namespace Skadmin\Slider\Doctrine\SliderItem;

use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Nettrine\ORM\EntityManagerDecorator;
use Skadmin\Slider\Doctrine\Slider\Slider;
use SkadminUtils\DoctrineTraits\Facade;

use function assert;

final class SliderItemFacade extends Facade
{
    use Facade\Sequence;

    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = SliderItem::class;
    }

    /**
     * @param array<mixed> $additionalSettings
     */
    public function create(Slider $slider, string $name, string $content, bool $isActive, string $url1, string $url2, string $urlText1, string $urlText2, array $additionalSettings, ?string $imagePreview): SliderItem
    {
        return $this->update(null, $slider, $name, $content, $isActive, $url1, $url2, $urlText1, $urlText2, $additionalSettings, $imagePreview);
    }

    /**
     * @param array<mixed> $additionalSettings
     */
    public function update(?int $id, Slider $slider, string $name, string $content, bool $isActive, string $url1, string $url2, string $urlText1, string $urlText2, array $additionalSettings, ?string $imagePreview): SliderItem
    {
        $sliderItem = $this->get($id);
        $sliderItem->update($slider, $name, $content, $isActive, $url1, $url2, $urlText1, $urlText2, $additionalSettings, $imagePreview);

        if (! $sliderItem->isLoaded()) {
            $sliderItem->setSequence($this->getValidSequence());
        }

        $this->em->persist($sliderItem);
        $this->em->flush();

        return $sliderItem;
    }

    public function get(?int $id = null): SliderItem
    {
        if ($id === null) {
            return new SliderItem();
        }

        $sliderItem = parent::get($id);

        if ($sliderItem === null) {
            return new SliderItem();
        }

        return $sliderItem;
    }

    /**
     * @return SliderItem[]
     */
    public function getAll(bool $onlyActive = false): array
    {
        $criteria = [];

        if ($onlyActive) {
            $criteria['isActive'] = true;
        }

        $orderBy = ['sequence' => 'ASC'];

        return $this->em
            ->getRepository($this->table)
            ->findBy($criteria, $orderBy);
    }

    public function getModel(?string $table = null): QueryBuilder
    {
        return parent::getModel($table)
            ->orderBy('a.sequence');
    }

    public function getModelForSlider(Slider $slider): QueryBuilder
    {
        $repository = $this->em
            ->getRepository($this->table);
        assert($repository instanceof EntityRepository);

        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('slider', $slider));

        return $repository->createQueryBuilder('a')
            ->orderBy('a.sequence')
            ->addCriteria($criteria);
    }
}
