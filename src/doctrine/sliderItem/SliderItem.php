<?php

declare(strict_types=1);

namespace Skadmin\Slider\Doctrine\SliderItem;

use Doctrine\ORM\Mapping as ORM;
use Skadmin\Slider\Doctrine\Slider\Slider;
use SkadminUtils\DoctrineTraits\Entity;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class SliderItem
{
    use Entity\Id;
    use Entity\Name;
    use Entity\Content;
    use Entity\IsActive;
    use Entity\ImagePreview;
    use Entity\Sequence;
    use Entity\AdditionalSettings;

    #[ORM\Column]
    private string $url1 = '';

    #[ORM\Column]
    private string $url2 = '';

    #[ORM\Column]
    private string $urlText1 = '';

    #[ORM\Column]
    private string $urlText2 = '';

    #[ORM\ManyToOne(targetEntity: Slider::class, inversedBy: 'items')]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private Slider $slider;

    /**
     * @param array<mixed> $additionalSettings
     */
    public function update(Slider $slider, string $name, string $content, bool $isActive, string $url1, string $url2, string $urlText1, string $urlText2, array $additionalSettings, ?string $imagePreview): void
    {
        $this->slider   = $slider;
        $this->name     = $name;
        $this->content  = $content;
        $this->isActive = $isActive;

        $this->url1     = $url1;
        $this->url2     = $url2;
        $this->urlText1 = $urlText1;
        $this->urlText2 = $urlText2;

        $this->additionalSettings = $additionalSettings;

        if ($imagePreview === null || $imagePreview === '') {
            return;
        }

        $this->imagePreview = $imagePreview;
    }

    public function getUrl1(): string
    {
        return $this->url1;
    }

    public function getUrl2(): string
    {
        return $this->url2;
    }

    public function getUrlText1(): string
    {
        return $this->urlText1;
    }

    public function getUrlText2(): string
    {
        return $this->urlText2;
    }

    public function getSlider(): Slider
    {
        return $this->slider;
    }
}
