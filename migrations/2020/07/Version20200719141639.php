<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200719141639 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $translations = [
            ['original' => 'form.slider.edit-item.url-text-1', 'hash' => '1cafdd8d6f667928fee800d93c92be53', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Odkaz #1 - titulek', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.slider.edit-item.url-text-2', 'hash' => '923aa716e1c5528de86f32700b0d67f0', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Odkaz #2 - titulek', 'plural1' => '', 'plural2' => ''],
        ];

        foreach ($translations as $translation) {
            $this->addSql('DELETE FROM translation WHERE hash = :hash', $translation);
            $this->addSql('SELECT create_translation(:original, :hash, :module, :language_id, :singular, :plural1, :plural2)', $translation);
        }
    }

    public function down(Schema $schema): void
    {
    }
}
