<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200426140105 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $translations = [
            ['original' => 'slider.overview', 'hash' => 'a24012ce2e4e80643765793011118b13', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Slider', 'plural1' => '', 'plural2' => ''],
            ['original' => 'role-resource.slider.title', 'hash' => 'e8a6670599ee5691139fc50dbb885609', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Slider', 'plural1' => '', 'plural2' => ''],
            ['original' => 'role-resource.slider.description', 'hash' => 'e83fcc593631c16e5b8ece81731b7d51', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Umožňuje spravovat slidery a jejich položky', 'plural1' => '', 'plural2' => ''],
            ['original' => 'slider.overview.title', 'hash' => 'a62bf84c84fa07b84b90cbd918628e12', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Slidery|Přehled', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.slider.overview.action.inline-add', 'hash' => '5e2ba752929f3575ec9315c16668b282', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Založit slider', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.slider.overview.name.req', 'hash' => 'c8da85170b79760bd7c2c00eca10b501', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zadejte prosím název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.slider.overview.is-active', 'hash' => '4d2b387061e19be084d9e7e7ef391a90', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Je aktivní?', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.slider.overview.name', 'hash' => '1f7457bee609915d420b15734d4ed2db', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.slider.overview.items-count', 'hash' => '5b94ba5cb1cf47bc58a5d8dd57064f1e', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Počet', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.slider.overview.action.overview-items', 'hash' => 'c0eb19419e56e62d7c04129e1d78740b', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Položky', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.slider.overview-items.is-active', 'hash' => 'c58884810844531ee5bd0218a7a70e60', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Je aktivní?', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.slider.overview-items.action.new', 'hash' => '7b27a1e79a6b1f1ad06056e92be93854', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Založit položku', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.slider.overview-items.name', 'hash' => 'd5dde71e6e7f0a3cae9a540275ae61f1', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.slider.overview-items.action.edit', 'hash' => '74a570e4c891cf579cbf534fead94365', 'module' => 'admin', 'language_id' => 1, 'singular' => '', 'plural1' => '', 'plural2' => ''],
            ['original' => 'slider.overview-items.title - %s', 'hash' => '7f0ce0958a92e96f1e70f339cf953a7b', 'module' => 'admin', 'language_id' => 1, 'singular' => '%s|Přehled položek slideru', 'plural1' => '', 'plural2' => ''],
            ['original' => 'slider.edit-item.title', 'hash' => 'bf6931256a508901cb0d555db71bd6b4', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Založení položky slideru', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.slider.edit-item.name', 'hash' => 'd293fcea2a350ee53e49b561be15cbc6', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.slider.edit-item.name.req', 'hash' => 'd5c286b3d8904b7b142fb8e5827973ad', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zadejte prosím název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.slider.edit-item.image-preview', 'hash' => 'f4229825dd28758f5b33588e6d9e4b27', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Obrázek slideru', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.slider.edit-item.image-preview.rule-image', 'hash' => 'b468ae1fa60829e4deade6d340f8813b', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Vybraný soubor není platný obrázek', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.slider.edit-item.url-1', 'hash' => '545ef39d33d739954b7845aeb1752826', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Odkaz #1 - url', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.slider.edit-item.url-2', 'hash' => '594e2640505c5d9fa1033ad41f4327aa', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Odkaz #2 - url', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.slider.edit-item.content', 'hash' => '0661851c48322b25968c66c50d27cfb6', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Obsah', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.slider.edit-item.is-active', 'hash' => '65bc70b9966e87352e151eaaa2d8e4c1', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Je aktivní', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.slider.edit-item.send', 'hash' => '76589245e322e32497c52cc9f95a6c32', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.slider.edit-item.send-back', 'hash' => 'f57632a106e7a3743918ab126c819645', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit a zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.slider.edit-item.back', 'hash' => '708f6f7c086fd50bb8dca8ed4f29bbda', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'slider.edit-item.title - %s', 'hash' => '4ee65b86e73d5dc6595150770a609a63', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Položka slideru %s|Editace', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.slider.edit-item.flash.success.create', 'hash' => 'a198c7578b675268e40be501260e927c', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Položka slideru byla úspěšně založena.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.slider.edit-item.flash.success.update', 'hash' => '35f2269bf4579c08aaee15dac8ed83d6', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Položka slideru byla úspěšně upravena.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.slider.overview-items.action.flash.sort.success', 'hash' => '8110d1006eaa31571b6dfa2422e4daca', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Pořadí položek slideru bylo upraveno.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.slider.overview.action.flash.inline-add.success "%s"', 'hash' => '36096658d1e431d0c01d36ddeb019e8e', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Slider "%s" byl úspěšně založen.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.slider.overview.action.flash.inline-edit.success "%s"', 'hash' => 'd01d86fd47f6ccc9a3825b020f72d90a', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Slider "%s" byl úspěšně upraven.', 'plural1' => '', 'plural2' => ''],
        ];

        foreach ($translations as $translation) {
            $this->addSql('DELETE FROM translation WHERE hash = :hash', $translation);
            $this->addSql('SELECT create_translation(:original, :hash, :module, :language_id, :singular, :plural1, :plural2)', $translation);
        }
    }

    public function down(Schema $schema): void
    {
    }
}
