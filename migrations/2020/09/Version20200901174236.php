<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200901174236 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $translations = [
            ['original' => 'form.slider.edit-item.url-text-1.placeholder', 'hash' => '36f900f5ac6b6bfd5b06b143ac32dc0a', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Více informací', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.slider.edit-item.url-text-2.placeholder', 'hash' => 'cff1fafe5c5fc8c7d3cc5fcc4296aaf9', 'module' => 'admin', 'language_id' => 1, 'singular' => '', 'plural1' => '', 'plural2' => ''],
        ];

        foreach ($translations as $translation) {
            $this->addSql('DELETE FROM translation WHERE hash = :hash', $translation);
            $this->addSql('SELECT create_translation(:original, :hash, :module, :language_id, :singular, :plural1, :plural2)', $translation);
        }
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
